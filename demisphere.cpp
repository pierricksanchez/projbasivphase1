#include "demisphere.hpp"
#include <iostream>
#include <fstream>

SphereX::SphereX(double x, double y, double z, double r)
    : m_x(x), m_y(y), m_z(z), m_radius(r)
{
}

void SphereX::readFromFile(const std::string& filename)
{
    std::ifstream inFile(filename);
    if (!inFile.is_open()) {
        std::cerr << "Erreur lors de l'ouverture du fichier " << filename << std::endl;
        return;
    }
    inFile >> m_x >> m_y >> m_z >> m_radius;
    inFile.close();
}

void SphereX::print() const
{
    std::cout << "SphereX: x=" << m_x << ", y=" << m_y << ", z=" << m_z << ", r=" << m_radius << std::endl;
}

double SphereX::getX() const
{
    return m_x;
}

double SphereX::getY() const
{
    return m_y;
}

double SphereX::getZ() const
{
    return m_z;
}

double SphereX::getRadius() const
{
    return m_radius;
}