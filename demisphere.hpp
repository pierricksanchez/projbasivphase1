#ifndef SPHEREX_HPP
#define SPHEREX_HPP

#include <string>

class SphereX {
public:
    SphereX(double x, double y, double z, double r);
    void readFromFile(const std::string& filename);
    void print() const;
    double getX() const;
    double getY() const;
    double getZ() const;
    double getRadius() const;

private:
    double m_x, m_y, m_z, m_radius;
};

#endif