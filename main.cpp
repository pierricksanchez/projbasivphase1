#include <iostream>
#include <thread>
#include <cmath>
#include "demisphere.hpp"

void calculateDistance(const SphereX& s1, const SphereX& s2)
{
    double dist = std::sqrt(std::pow(s1.getX() - s2.getX(), 2) +
                            std::pow(s1.getY() - s2.getY(), 2) +
                            std::pow(s1.getZ() - s2.getZ(), 2));
    std::cout << "Distance entre les deux spheres: " << dist << std::endl;
    if (dist <= s1.getRadius() + s2.getRadius()) {
        std::cout << "Les deux spheres sont en contact" << std::endl;
    }
}

int main()
{
    SphereX s1(0, 0, 0, 0), s2(0, 0, 0, 0);

    // lecture des données de la première demi-sphère depuis un fichier
    std::thread t1([&s1](){
        s1.readFromFile("sphere1.txt");
        s1.print();
    });

    // lecture des données de la deuxième demi-sphère depuis un fichier
    std::thread t2([&s2](){
        s2.readFromFile("sphere2.txt");
        s2.print();
    });

    t1.join();
    t2.join();

    // calcul de la distance et détection de contact
    std::thread t3(calculateDistance, std::ref(s1), std::ref(s2));
    t3.join();

    return 0;
}